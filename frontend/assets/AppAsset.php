<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'css/site.css',
        '/css/bootstrap.min.css',
        '/css/reset.css',
        '/css/font-awesome.min.css',
        '/css/style.css',
        '/fw/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
    ];
    public $js = [
        // 'js/jquery-2.2.3.min.js',
        'js/moment.js',
        'js/mycode.js',
        'js/prism.js',
        '/fw/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
    ];
    public $depends = [
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
