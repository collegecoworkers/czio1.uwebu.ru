<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\Registry;
use common\models\Doktor;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Registry();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $model->full_name = Yii::$app->request->post()['Registry']['full_name'];
            $model->phone = Yii::$app->request->post()['Registry']['phone'];
            $model->policy = Yii::$app->request->post()['Registry']['policy'];
            $model->email = Yii::$app->request->post()['Registry']['email'];
            $model->doktor_id = Yii::$app->request->post()['Registry']['doktor_id'];
            $model->datetime = Yii::$app->request->post()['Registry']['datetime'];

            $model->save();

            return $this->render('success');
        }

        $doktors = [];
        $all_doktors = Doktor::find()->all();
        
        for ($i=0; $i < count($all_doktors); $i++) { 
            $doktors[$all_doktors[$i]->id] = $all_doktors[$i]->full_name;
        }

        return $this->render('index', [
            'model' => $model,
            'doktors' => $doktors
        ]);
    }

    public function actionCheck()
    {
        $model = new Registry();
        return $this->render('check', [
            'model' => $model,
        ]);
    }
}
