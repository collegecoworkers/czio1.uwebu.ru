<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */

$this->title = 'Регистратура';
?>
<div class="post-border shadow">
    <div class="panel panel-default post-panel">
        <div class="panel-body">
            <div class="post-title">Просмотреть все записи</div>
            <div class="post-text">
                <!-- -->
                <?php $form = ActiveForm::begin() ?>
                
                <?= $form->field($model, 'policy')->textInput(['maxlength'=>true, 'placeholder' => 'Номер полиса']) ?>

                <?= Html::submitButton('Проверить',['class' => 'submit']) ?>
                <?php ActiveForm::end() ?>
                <!-- -->
            </div>
        </div>
    </div>
</div>