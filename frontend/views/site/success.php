<?php

use yii\helpers\Html;

$this->title = 'Успех';

?>
<div class="site-error">

    <div class="alert alert-success">
        <h1>
            <?= nl2br(Html::encode('Вы были успешно записанны на прием')) ?>
        </h1>
    </div>

</div>
