<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
use dosamigos\datetimepicker\DateTimePicker;

/* @var $this yii\web\View */

$this->title = 'Регистратура';
?>
<div class="post-border shadow">
    <div class="panel panel-default post-panel">
        <div class="panel-body">
            <div class="post-title">Запись на прием</div>
            <div class="post-text">
                <!-- -->
                <?php $form = ActiveForm::begin() ?>
    
                <?= $form->field($model, 'full_name')->textInput(['placeholder' => 'ФИО']) ?>
                <?= $form->field($model, 'policy')->textInput(['maxlength'=>true, 'placeholder' => 'Номер полиса']) ?>
                <?= $form->field($model, 'phone')->widget(MaskedInput::className(),['mask' => '+7 (999) 999-9999'])->textInput(['placeholder' => 'Номер телефона']) ?>
                <?= $form->field($model, 'email')->textInput(['placeholder' => 'Электронный адрес']) ?>
                <?= $form->field($model, 'doktor_id')->dropDownList($doktors) ?>
                <?= $form->field($model, 'datetime')->textInput(['u-language'=>'ru','placeholder' => 'Дата и время приема', 'class' => 'form-control datetimepicker']) ?>

                <?= Html::submitButton('Отправить',['class' => 'submit']) ?>
                <?php ActiveForm::end() ?>
                <!-- -->
            </div>
        </div>
    </div>
</div>