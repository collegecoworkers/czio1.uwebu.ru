<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<!-- header -->
<div class="">
    <div class="header">
        <div class="h-title"><?=  $this->title ?></div>
        <div class="h-menu-box">
            <ul>
                <li><a href="/">Записаться на прием</a></li>
                <!-- <li><a href="/site/check">Проверить запись</a></li> -->
            </ul>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!-- /header -->
<div class="container" id="wrap">
    <div class="row" style="margin-top: 35px;">
        <div class="col-sm-12" style=" line-height=> 0px;">
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>
</div>
<div class="footer" style="text-align: center">
    <span>&copy; My Company <?= date('Y') ?></span>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
