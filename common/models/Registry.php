<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Registry model
 *
 * @property integer $id
 * @property string $email
 */
class Registry extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%registry}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name', 'policy','datetime'], 'required'],
            ['email', 'email'],
            ['datetime', 'datetime', 'format' => '20y-m-d H:m:s'],
            ['policy', 'string', 'max' => 10],
            ['phone', 'string', 'max' => 20],
            [['status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * Finds registry by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByRegistryname($username)
    {
        // need to do like
        return static::findOne(['username' => $username,]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

}
