<?php

use yii\db\Migration;

class m171004_101859_registry extends Migration
{
    public function safeUp()
    {
        $this->createTable('registry', [
            'id' => $this->primaryKey(),
            'full_name' => $this->string(500),
            'phone' => $this->string(15),
            'policy' => $this->string(10),
            'email' => $this->string(32),
            'doktor_id' => $this->integer()->notNull(),
            'datetime' => $this->dateTime(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
       // creates index for column `doktor_id`
        $this->createIndex(
            'idx-registry-doktor_id',
            'registry',
            'doktor_id'
        );

        // add foreign key for table `doktor`
        $this->addForeignKey(
            'fk-registry-doktor_id',
            'registry',
            'doktor_id',
            'doktor',
            'id',
            'CASCADE'
        );

    }

    public function safeDown()
    {
        echo "m171004_101859_registry cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171004_101859_registry cannot be reverted.\n";

        return false;
    }
    */
}
