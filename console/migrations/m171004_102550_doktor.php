<?php

use yii\db\Migration;

class m171004_102550_doktor extends Migration
{
    public function safeUp()
    {
        $this->createTable('doktor', [
            'id' => $this->primaryKey(),
            'full_name' => $this->string(500),
            'email' => $this->string(32),
            'specification' => $this->text(),
            // 'freedatetime' => $this->dateTime(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    public function safeDown()
    {
        echo "m171004_102550_doktor cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171004_102550_doktor cannot be reverted.\n";

        return false;
    }
    */
}
