<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use backend\models\Signup;
use backend\models\Login;
use backend\models\UserDoktor;
use common\models\Registry;
use common\models\Doktor;

class SiteController extends Controller
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['signup', 'login', 'error'],
						'allow' => true,
					],
					[
						'actions' => [
							'logout',
							'index',
							'update',
							'delete',
						],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionUsers()
	{
		$model = User::find();
		return $this->render('users', [
			'model' => $model
		]);
	}

	public function actionIndex()
	{
		$model = Registry::find();

		if (Yii::$app->session->get('dok'))
			$model = Registry::find()->where(['doktor_id' => Yii::$app->session->get('dok')]);

		return $this->render('index', ['model' => $model]);
	}
	
	public function actionUpdate($id)
	{
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Registry::findIdentity($id);

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->save();
			return $this->redirect(['index']);
		}
		
		$doktors = [];
		$all_doktors = Doktor::find()->all();

		for ($i=0; $i < count($all_doktors); $i++) { 
			$doktors[$all_doktors[$i]->id] = $all_doktors[$i]->full_name;
		}

		return $this->render('update', [
			'model' => $model,
			'doktors' => $doktors,
		]);
	}

	public function actionDelete($id)
	{
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Registry::findIdentity($id);
		$model->delete();
		return $this->redirect(['index']);
	}

	// public function actionSignup()
	// {
	// 	if (!Yii::$app->user->isGuest) {
	// 		return $this->goHome();
	// 	}

	// 	$model = new Signup();

	// 	if(isset($_POST['Signup']))
	// 	{
	// 		$model->attributes = Yii::$app->request->post('Signup');
	// 		if($model->validate() && $model->signup())
	// 		{
	// 			return $this->redirect(['index']);
	// 		}
	// 	}

	// 	return $this->render('signup', [
	// 		'model' => $model,
	// 	]);
	// }

	public function actionLogin()
	{
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Login();

		if( Yii::$app->request->post('Login')) {
			$model->attributes = Yii::$app->request->post('Login');
			if($model->validate()) {
				$user = $model->getUser();

				if($user->is_dok != 0){
					Yii::$app->session->set('dok', $user->is_dok);
				}

				Yii::$app->user->login($user);

				return $this->goHome();
			}
		}

		return $this->render('login', [
			'model' => $model,
		]);
	}

	public function actionLogout()
	{
		Yii::$app->session->remove('dok');
		Yii::$app->user->logout();
		return $this->goHome();
	}
}
