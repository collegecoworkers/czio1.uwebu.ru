<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\UserDoktor;
use common\models\User;
use common\models\Doktor;

class UserController extends Controller
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['error'],
						'allow' => true,
					],
					[
						'actions' => ['index', 'update', 'delete', 'create'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex()
	{
		$model = User::find();
		return $this->render('index', [
			'model' => $model,
		]);
	}
	
	public function actionUpdate($id)
	{
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = User::findIdentity($id);

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->is_dok = Yii::$app->request->post()['User']['is_dok'];
			$model->save();
			return $this->redirect(['index']);
		}

		$doktors = [];
		$all_doktors = Doktor::find()->all();

		$doktors[0] = 'Админ';
		for ($i=0; $i < count($all_doktors); $i++) { 
			$doktors[$all_doktors[$i]->id] = $all_doktors[$i]->full_name;
		}

		return $this->render('update', [
			'model' => $model,
			'doktors' => $doktors
		]);
	}

	public function actionCreate()
	{
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new User();

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {

			$model->username = Yii::$app->request->post()['User']['username'];
			$model->email = Yii::$app->request->post()['User']['email'];
			$model->setPassword(Yii::$app->request->post()['User']['password_hash']);

			$model->save();
			return $this->redirect(['index']);
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	public function actionDelete($id)
	{
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = User::findIdentity($id);
		$model->delete();
		return $this->redirect(['index']);
	}

}
