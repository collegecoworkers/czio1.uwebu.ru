<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Doktor;

class DoktorController extends Controller
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['error'],
						'allow' => true,
					],
					[
						'actions' => ['index', 'update', 'delete', 'create'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex()
	{
		$model = new Doktor();
		return $this->render('index', [
			'model' => $model
		]);
	}
	
	public function actionUpdate($id)
	{
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Doktor::findIdentity($id);

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->save();
			return $this->redirect(['index']);
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	public function actionCreate()
	{
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Doktor();

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {

			$model->full_name = Yii::$app->request->post()['Doktor']['full_name'];
			$model->email = Yii::$app->request->post()['Doktor']['email'];
			$model->specification = Yii::$app->request->post()['Doktor']['specification'];
			$model->save();
			return $this->redirect(['index']);
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	public function actionDelete($id)
	{
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Doktor::findIdentity($id);
		$model->delete();
		return $this->redirect(['index']);
	}
}
