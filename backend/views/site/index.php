<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Registry;
use common\models\Doktor;
use yii\data\ActiveDataProvider;

$this->title = Yii::t('app', 'Заявки');
$this->params['breadcrumbs'][] = $this->title;
$status = '';
?>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?= $this->title ?></div>
		<div class="panel-body">
<div class="contact-index">
	<div class="fa-br"></div>
	<br>
	<?php
	$dataProvider = new ActiveDataProvider([
		'query' => $model,
		'pagination' => [
		 'pageSize' => 20,
		],
	]);
	echo GridView::widget([
		'dataProvider' => $dataProvider,
		'layout' => "{items}\n{pager}",
		'columns' => [
			// ['class' => 'yii\grid\SerialColumn'],
			'id',
			[
				'label' => 'ФИО',
				'attribute' => 'full_name',
				'format' => 'raw',
				'value' => function($dataProvider){
					return $dataProvider->full_name;
				},
			],
			[
				'label' => 'Телефон',
				'attribute' => 'phone',
				'format' => 'raw',
				'value' => function($dataProvider){
					return $dataProvider->phone;
				},
			],
			[
				'label' => 'Полис',
				'attribute' => 'policy',
				'format' => 'raw',
				'value' => function($dataProvider){
					return $dataProvider->policy;
				},
			],
			'email:ntext',
			[
				'label' => 'Доктор',
				'attribute' => 'doktor_id',
				'format' => 'raw',
				'value' => function($dataProvider){
					$doc = Doktor::findIdentity($dataProvider->doktor_id);
					return Html::a($doc->full_name ,['/doktor']);
				},
				'visible' => (!Yii::$app->session->get('dok')),
			],
			[
				'label' => 'Дата и Время',
				'attribute' => 'datetime',
				'format' => 'raw',
				'value' => function($dataProvider){
					return $dataProvider->datetime;
				},
			],
			[
				'label' => 'Статус',
				'attribute' => 'status',
				'format' => 'raw',
				'value' => function($dataProvider){
					switch ($dataProvider->status) {
						case '0': return 'В ожидании';
						case '1': return 'Готов';
						default: return 'Отменен';
					}
				},
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'header'=>'Действия', 
				'headerOptions' => ['width' => '80'],
				'template' => '{update} {delete}{link}',
			],
		],
	]);
	?>

</div>

		</div>
	</div>
</div>
