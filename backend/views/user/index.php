<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\Registry;
use common\models\Doktor;

/* @var $this yii\web\View */
/* @var $dataProvider backend\modules\contact\models\Contact */
/* @var $searchModel backend\modules\contact\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Пользователи');
$this->params['breadcrumbs'][] = $this->title;

$status = '';
?>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?= $this->title ?></div>
		<div class="panel-body">

<div class="contact-index">
<?= Html::a(Yii::t('app','Добавить'), Url::base() . '/user/create') ?>
	<div class="fa-br"></div>
	<br>
	<?php

	// use yii\grid\GridView;
	use yii\data\ActiveDataProvider;

	$dataProvider = new ActiveDataProvider([
		'query' => $model,
		'pagination' => [
		 'pageSize' => 20,
		],
	]);

	echo GridView::widget([
		'dataProvider' => $dataProvider,
		'layout' => "{items}\n{pager}",
		'columns' => [
			// ['class' => 'yii\grid\SerialColumn'],
			'id',
			[
				'label' => 'Ник',
				'attribute' => 'username',
				'format' => 'raw',
				'value' => function($dataProvider){
					return $dataProvider->username;
				},
			],
			'email:ntext',
			[
				'label' => 'Доктор',
				'attribute' => 'is_dok',
				'format' => 'raw',
				'value' => function($dataProvider){
					if($dataProvider->is_dok != 0){
						$doc = Doktor::findIdentity($dataProvider->is_dok);
						return $doc->full_name;
					}
					return 'Нет';
				},
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'header'=>'Действия', 
				'headerOptions' => ['width' => '80'],
				'template' => '{update} {delete}{link}',
			],
		],
	]);
	?>

</div>

		</div>
	</div>
</div>
