<?php

use yii\helpers\Html;
use backend\components\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use dosamigos\datetimepicker\DateTimePicker;

$this->title = Yii::t('app', 'Юзер');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading"><?= $model->username ?></div>
        <div class="panel-body">

<?php

$this->title = Yii::t('app', 'Отредактировать');

$this->params['breadcrumbs'][] = ['label' => $model->id];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="link-update">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'username')->textInput(['placeholder' => 'Ник']) ?>
	<?= $form->field($model, 'email')->textInput(['placeholder' => 'Электронный адрес']) ?>
	<?= $form->field($model, 'is_dok')->dropDownList($doktors) ?>

	<div class="form-group center">
		<?= ''// Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' =>  'btn btn-success']) ?>
	</div>
	<?php ActiveForm::end(); ?>


</div>

        </div>
    </div>
</div>
