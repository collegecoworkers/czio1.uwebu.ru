<?php

use yii\helpers\Html;
use backend\components\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use dosamigos\datetimepicker\DateTimePicker;

use common\models\Doktor;
use common\models\Registry;

/* @var $this yii\web\View */
/* @var $dataProvider backend\modules\contact\models\Contact */
/* @var $searchModel backend\modules\contact\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Новый пользователь');
$this->params['breadcrumbs'][] = $this->title;

$status = '';
?>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading"><?= $this->title ?></div>
        <div class="panel-body">

<?php



$this->title = Yii::t('app', 'Создание нового доктора');

?>
<div class="link-update">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'username')->textInput(['placeholder' => 'ФИО']) ?>
	<?= $form->field($model, 'email')->textInput(['placeholder' => 'Электронный адрес']) ?>
	<?= $form->field($model, 'password_hash')->passwordInput() ?>

	<div class="form-group center">
		<?= Html::submitButton(Yii::t('app', 'Добавить'), ['class' =>  'btn btn-success']) ?>
	</div>
	<?php ActiveForm::end(); ?>


</div>

        </div>
    </div>
</div>
