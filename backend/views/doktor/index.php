<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\Registry;
use common\models\Doktor;

/* @var $this yii\web\View */
/* @var $dataProvider backend\modules\contact\models\Contact */
/* @var $searchModel backend\modules\contact\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Доктора');
$this->params['breadcrumbs'][] = $this->title;

$status = '';
?>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?= $this->title ?></div>
		<div class="panel-body">

<div class="contact-index">
<?= Html::a(Yii::t('app','Добавить'), Url::base() . '/doktor/create') ?>
	<div class="fa-br"></div>
	<br>
	<?php

	// use yii\grid\GridView;
	use yii\data\ActiveDataProvider;

	$dataProvider = new ActiveDataProvider([
		'query' => Doktor::find(),
		'pagination' => [
		 'pageSize' => 20,
		],
	]);

	echo GridView::widget([
		'dataProvider' => $dataProvider,
		'layout' => "{items}\n{pager}",
		'columns' => [
			// ['class' => 'yii\grid\SerialColumn'],
			'id',
			[
				'label' => 'ФИО',
				'attribute' => 'full_name',
				'format' => 'raw',
				'value' => function($dataProvider){
					return $dataProvider->full_name;
				},
			],
			'email:ntext',
			[
				'label' => 'Кол-во записей',
				'attribute' => 'registers',
				'format' => 'raw',
				'value' => function($dataProvider){
					return count(Registry::findAll(['doktor_id' => $dataProvider->id]));
				},
			],
			[
				'label' => 'Профессия',
				'attribute' => 'specification',
				'format' => 'raw',
				'value' => function($dataProvider){
					return $dataProvider->specification;
				},
			],
			[
				'label' => 'Поcледняя запись',
				'attribute' => 'last',
				'format' => 'raw',
				'value' => function($dataProvider){
					return Registry::find()
												->where(['doktor_id' => $dataProvider->id])
												->orderBy(['id' => SORT_DESC])
												->one()->datetime;
				},
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'header'=>'Действия', 
				'headerOptions' => ['width' => '80'],
				'template' => '{update} {delete}{link}',
			],
		],
	]);
	?>

</div>

		</div>
	</div>
</div>
