<?php

use yii\helpers\Html;
use backend\components\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use dosamigos\datetimepicker\DateTimePicker;

use common\models\Doktor;
use common\models\Registry;

/* @var $this yii\web\View */
/* @var $dataProvider backend\modules\contact\models\Contact */
/* @var $searchModel backend\modules\contact\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Доктор');
$this->params['breadcrumbs'][] = $this->title;

$status = '';
?>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading"><?= $model->full_name ?></div>
        <div class="panel-body">

<?php



$this->title = Yii::t('app', 'Update');

$this->params['breadcrumbs'][] = ['label' => $model->id];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="link-update">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'full_name')->textInput(['placeholder' => 'ФИО']) ?>
	<?= $form->field($model, 'email')->textInput(['placeholder' => 'Электронный адрес']) ?>
	<?= $form->field($model, 'specification')->textInput(['placeholder' => 'Профессия']) ?>

	<div class="form-group center">
		<?= ''// Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<?= Html::submitButton(Yii::t('app', 'Update'), ['class' =>  'btn btn-success']) ?>
	</div>
	<?php ActiveForm::end(); ?>


</div>

        </div>
    </div>
</div>
