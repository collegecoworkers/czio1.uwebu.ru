<?php

use backend\assets\AppAsset;
use common\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);

$modules_id = Yii::$app->controller->module->id;
$controller_id = Yii::$app->controller->id;
$action_id = Yii::$app->controller->action->id;
$entity_id = isset($_GET['id']) ? (int)$_GET['id'] : null;

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <script type="text/javascript">
            var base_url = '<?= Url::base() ?>';
            var modules_name = '<?= $modules_id ?>';
            var controller_name = '<?= $controller_id ?>';
            var action_name = '<?= $action_id ?>';
            var entity_id = '<?= $entity_id ?>';
        </script>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="header">
        <div class="header-split" style="float: left; margin-top: 18px;">
        <div class="raspina-logo"><?= Html::a('Админ панель', ['/']) ?></div>
        </div>

<!--         <div class="header-split" style="margin-top: 8px;">
            <div class="dropdown">
                <img class="user-profile dropdown-toggle" data-toggle="dropdown" src="<?= '' // User::getAvatar(Yii::$app->user->id); ?>">
                <ul class="dropdown-menu dropdown-menu-right raspina-profile ">
                    <div class="user-profile-h" style="">
                        <a href="<?= '' //Url::base(); ?>/user/default/avatar"><img class="user-profile-big" src="<?= ''//User::getAvatar(Yii::$app->user->id); ?>"></a>
                        <div class="user-profile-name"><?= ''//Yii::$app->user->identity->username ?></div>
                        <div class="user-profile-name"><?= ''//Yii::$app->user->identity->email ?></div>
                        <a href="/site/login">Выход</a>
                    </div>
                </ul>
            </div>
        </div> -->
    <?php 
        $header_split_s = [
            [
                'url'  => '/site/logout', 
                'dok'  => true, 
                'icon' => 'sign-out',
                'label' => 'Выход',
            ],
            [
                'url'  => '/doktor/index', 
                'dok'  => false, 
                'icon' => 'user-md',
                'label' => 'Список докторов',
            ],
            [
                'url'  => '/user', 
                'dok'  => false, 
                'icon' => 'user',
                'label' => 'Список пользователей',
            ],
            [
                'url'  => '/', 
                'dok'  => true, 
                'icon' => 'home',
                'label' => 'Главная',
            ],
        ];    
     ?>
  <!--       <div class="header-split">
            <div class="dropdown">
                <span class="fa fa-bars dropdown-toggle" data-toggle="dropdown"></span>
                <ul class="dropdown-menu dropdown-menu-right raspina-menu">
                    <?php foreach ($header_split_s as $header_split): ?>
                    <li><a href="<?= Url::base() . $header_split['url'] ?>">
                            <div><span class="fa fa-<?= $header_split['icon'] ?>"></span></div>
                            <div class="menu-title"><?= $header_split['label'] ?></div>
                        </a>
                    </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div> -->

    <?php if (!Yii::$app->user->isGuest): ?>
            <?php foreach ($header_split_s as $header_split): ?>
                <?php
                    if(Yii::$app->session->get('dok') && !$header_split['dok']) continue; 
                ?>
                <div class="header-split" style="margin-top: 8px;">
                    <a href="<?= Url::base() . $header_split['url'] ?>">
                        <div><span class="fa fa-<?= $header_split['icon'] ?>"></span></div>
                        <div class="menu-title"><?= $header_split['label'] ?></div>
                    </a>
                </div>
            <?php endforeach ?>
    <?php endif ?>

    </div>


    <div class="pull-left main-content">
        <div class="col-md-12">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
        </div>
        <?= $content ?>
    </div>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>